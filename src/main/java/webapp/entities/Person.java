/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author dom
 */
public class Person {
    
    private Integer id;
    private String name;
    private String surname;
    private String gender;
    private LocalDate dateOfBirth; 
    private GovernmentID governmentID;
    private ContactInformation contactInformation;
    private Country country;
    private String Citizenship;
    private String marriageStatus;
    
    public Person (){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public GovernmentID getGovernmentID() {
        return governmentID;
    }

    public void setGovernmentID(GovernmentID governmentID) {
        this.governmentID = governmentID;
    }

    public ContactInformation getContactInformation() {
        return contactInformation;
    }

    public void setContactInformation(ContactInformation contactInformation) {
        this.contactInformation = contactInformation;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCitizenship() {
        return Citizenship;
    }

    public void setCitizenship(String Citizenship) {
        this.Citizenship = Citizenship;
    }

    public String getMarriageStatus() {
        return marriageStatus;
    }

    public void setMarriageStatus(String marriageStatus) {
        this.marriageStatus = marriageStatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.surname);
        hash = 79 * hash + Objects.hashCode(this.gender);
        hash = 79 * hash + Objects.hashCode(this.dateOfBirth);
        hash = 79 * hash + Objects.hashCode(this.governmentID);
        hash = 79 * hash + Objects.hashCode(this.contactInformation);
        hash = 79 * hash + Objects.hashCode(this.country);
        hash = 79 * hash + Objects.hashCode(this.Citizenship);
        hash = 79 * hash + Objects.hashCode(this.marriageStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.surname, other.surname)) {
            return false;
        }
        if (!Objects.equals(this.gender, other.gender)) {
            return false;
        }
        if (!Objects.equals(this.Citizenship, other.Citizenship)) {
            return false;
        }
        if (!Objects.equals(this.marriageStatus, other.marriageStatus)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateOfBirth, other.dateOfBirth)) {
            return false;
        }
        if (!Objects.equals(this.governmentID, other.governmentID)) {
            return false;
        }
        if (!Objects.equals(this.contactInformation, other.contactInformation)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", gender=" + gender + ", dateOfBirth=" + dateOfBirth + ", governmentID=" + governmentID + ", contactInformation=" + contactInformation + ", country=" + country + ", Citizenship=" + Citizenship + ", marriageStatus=" + marriageStatus + '}';
    }
    
    
}
