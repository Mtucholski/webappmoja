/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public class Italy extends Country {

    private String administrationRegion;
    private Country city;
    private Country countryName;
    private Country village;
    

    public Italy() {
    }

    public String getAdministrationRegion() {
        return administrationRegion;
    }

    public void setAdministrationRegion(String administrationRegion) {
        this.administrationRegion = administrationRegion;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.administrationRegion);
        hash = 89 * hash + Objects.hashCode(this.city);
        hash = 89 * hash + Objects.hashCode(this.countryName);
        hash = 89 * hash + Objects.hashCode(this.village);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Italy other = (Italy) obj;
        if (!Objects.equals(this.administrationRegion, other.administrationRegion)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        return true;
    }

    

    @Override
    public String toString() {
        return "Italy{" + "administrationRegion=" + administrationRegion + ", city=" + city + ", countryName=" + countryName + ", village=" + village + '}';
    }

    

}
