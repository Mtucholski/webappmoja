/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author dom
 */
public class GovernmentID {

    private ArrayList<GovernmentID> governmentIDDocumentType;
    private String governmentIDNumber;
    private LocalDate dateOfIssue;
    private LocalDate expiryDate;
    
    //constructor needed for hibernate framework
    public GovernmentID(){
    }

    // getters and setters

    public ArrayList<GovernmentID> getGovernmentIDDocumentType() {
        return governmentIDDocumentType;
    }

    public void setGovernmentIDDocumentType(ArrayList<GovernmentID> governmentIDDocumentType) {
        this.governmentIDDocumentType = governmentIDDocumentType;
    }

    public String getGovernmentIDNumber() {
        return governmentIDNumber;
    }

    public void setGovernmentIDNumber(String governmentIDNumber) {
        this.governmentIDNumber = governmentIDNumber;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }
// equals and hashcode needed for checking if governmentId already exist
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.governmentIDDocumentType);
        hash = 97 * hash + Objects.hashCode(this.governmentIDNumber);
        hash = 97 * hash + Objects.hashCode(this.dateOfIssue);
        hash = 97 * hash + Objects.hashCode(this.expiryDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GovernmentID other = (GovernmentID) obj;
        if (!Objects.equals(this.governmentIDNumber, other.governmentIDNumber)) {
            return false;
        }
        if (!Objects.equals(this.governmentIDDocumentType, other.governmentIDDocumentType)) {
            return false;
        }
        if (!Objects.equals(this.dateOfIssue, other.dateOfIssue)) {
            return false;
        }
        if (!Objects.equals(this.expiryDate, other.expiryDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "GovernmentID{" + "governmentIDDocumentType=" + governmentIDDocumentType + ", governmentIDNumber=" + governmentIDNumber + ", dateOfIssue=" + dateOfIssue + ", expiryDate=" + expiryDate + '}';
    }
    
    
    
    
}
