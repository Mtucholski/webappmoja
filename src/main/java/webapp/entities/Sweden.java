/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public class Sweden extends Country {
    
    private String län;
    private Country city;
    private Country countryName;
    private Country village;

    public Sweden() {
    }

    public String getLän() {
        return län;
    }

    public void setLän(String län) {
        this.län = län;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.län);
        hash = 97 * hash + Objects.hashCode(this.city);
        hash = 97 * hash + Objects.hashCode(this.countryName);
        hash = 97 * hash + Objects.hashCode(this.village);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sweden other = (Sweden) obj;
        if (!Objects.equals(this.län, other.län)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Sweden{" + "l\u00e4n=" + län + ", city=" + city + ", countryName=" + countryName + ", village=" + village + '}';
    }

    
    
}
