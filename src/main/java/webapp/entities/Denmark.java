/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public class Denmark extends Country {

    private String land;
    private Country city;
    private Country countryName;
    private Country village;

    public Denmark() {
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        this.land = land;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.land);
        hash = 59 * hash + Objects.hashCode(this.city);
        hash = 59 * hash + Objects.hashCode(this.countryName);
        hash = 59 * hash + Objects.hashCode(this.village);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Denmark other = (Denmark) obj;
        if (!Objects.equals(this.land, other.land)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Denmark{" + "land=" + land + ", city=" + city + ", countryName=" + countryName + ", village=" + village + '}';
    }

    
    
    

}
