/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public class Switzerland extends Country {
    
    private String cantons;
    private Country city;
    private Country countryName;
    private Country village;

    public Switzerland() {
    }

    public String getCantons() {
        return cantons;
    }

    public void setCantons(String cantons) {
        this.cantons = cantons;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.cantons);
        hash = 23 * hash + Objects.hashCode(this.city);
        hash = 23 * hash + Objects.hashCode(this.countryName);
        hash = 23 * hash + Objects.hashCode(this.village);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Switzerland other = (Switzerland) obj;
        if (!Objects.equals(this.cantons, other.cantons)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Switzerland{" + "cantons=" + cantons + ", city=" + city + ", countryName=" + countryName + ", village=" + village + '}';
    }

    
   
}
