/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public  class Country {
    
    protected String countryName;
    protected String city;
    protected String village;
    protected String town;

    public Country() {
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.countryName);
        hash = 41 * hash + Objects.hashCode(this.city);
        hash = 41 * hash + Objects.hashCode(this.village);
        hash = 41 * hash + Objects.hashCode(this.town);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Country other = (Country) obj;
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        if (!Objects.equals(this.town, other.town)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Country{" + "countryName=" + countryName + ", city=" + city + ", village=" + village + ", town=" + town + '}';
    }
    
}

   

   
