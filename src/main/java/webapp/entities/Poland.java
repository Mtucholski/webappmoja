/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webapp.entities;

import java.util.Objects;

/**
 *
 * @author dom
 */
public class Poland extends Country  {
    
    private String district;
    private String county;
    private String community;
    private Country city;
    private Country countryName;
    private Country village;
    
    
    
    public Poland (){
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    
    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.district);
        hash = 89 * hash + Objects.hashCode(this.county);
        hash = 89 * hash + Objects.hashCode(this.community);
        hash = 89 * hash + Objects.hashCode(this.city);
        hash = 89 * hash + Objects.hashCode(this.countryName);
        hash = 89 * hash + Objects.hashCode(this.village);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Poland other = (Poland) obj;
        if (!Objects.equals(this.district, other.district)) {
            return false;
        }
        if (!Objects.equals(this.county, other.county)) {
            return false;
        }
        if (!Objects.equals(this.community, other.community)) {
            return false;
        }
        if (!Objects.equals(this.city, other.city)) {
            return false;
        }
        if (!Objects.equals(this.countryName, other.countryName)) {
            return false;
        }
        if (!Objects.equals(this.village, other.village)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Poland{" + "district=" + district + ", county=" + county + ", community=" + community + ", city=" + city + ", countryName=" + countryName + ", village=" + village + '}';
    }

   
   
}
